// index.js
import en from './lang/en.json'
import fr from './lang/fr.json'

export const defaultLocale = 'en'

export const languages = {
    en: en,
    fr: fr,
}

