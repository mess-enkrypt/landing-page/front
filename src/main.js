import Vue from 'vue'
import VueRouter from 'vue-router'
import VueI18n from 'vue-i18n'
import App from './App.vue'
import './registerServiceWorker'
import Buefy from 'buefy'
import 'buefy/dist/buefy.css'
import axios from 'axios'
import VueAxios from 'vue-axios'

import Home from './components/Home.vue'
import About from './components/About.vue'
import RegisterBetaForm from './components/RegisterBetaForm.vue'
import UnRegisterBetaForm from './components/UnRegisterBetaForm.vue'
import ReportBugForm from './components/ReportBugForm.vue'
import Downloads from './components/Downloads.vue'
import PrivacyPolicy from './components/PrivacyPolicy.vue'
import Bot from './components/Bot.vue'
import Toasted from 'vue-toasted'

Vue.use(Toasted)
Vue.use(VueAxios, axios)
Vue.use(VueRouter)
Vue.use(VueI18n)
Vue.use(Buefy);

import { languages } from './i18n/index.js'
import { defaultLocale } from './i18n/index.js'
const messages = Object.assign(languages)

Vue.config.productionTip = false

// Create VueI18n instance with options
var i18n = new VueI18n({
    locale: defaultLocale, // set locale
    fallbackLocale: 'en',
    messages, // set locale messages
})

const routes = [
    { path: '/', component: Home },
    { path: '/about', component: About },
    { path: '/beta/report', component: ReportBugForm },
    { path: '/beta/register', component: RegisterBetaForm },
    { path: '/beta/unregister', component: UnRegisterBetaForm },
    { path: '/downloads', component: Downloads },
    { path: '/demo', component: Bot },
    { path: '/privacy-policy', component: PrivacyPolicy }
]

const router = new VueRouter({
    mode: 'history',
    routes: routes // short for `routes: routes`
})

new Vue({
    i18n,
    router,
    render: h => h(App)
}).$mount('#app')
