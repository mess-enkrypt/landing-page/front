# MessEnkrypt Landing Page

Requirements:
   - docker
   - docker-compose
   - make

Launch attached in term:
	```make dev```

Launch:
	```make start```

Stop:
	```make stop```

Delete containers:
	```make clean```

Install docker:
	```make install_dc```

Install docker-compose:
	```m̀ake install_dcc```

Logs:
	```make logs```
