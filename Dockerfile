FROM node:10.15.0-alpine
WORKDIR /usr/src/app

RUN apk update
RUN apk add python git
#Install PM2 Global
#RUN npm install -g pm2

COPY package*.json ./
RUN npm i

COPY .env* ./

CMD ["npm","run", "serve"]
