module.exports = {
    "env": {
        "browser": true,
        "es6": true
    },
    "extends": [
        "eslint:recommended",
        "plugin:vue/essential"
    ],
    "globals": {
        "Atomics": "readonly",
        "SharedArrayBuffer": "readonly",
	"no-mixed-spaces-and-tabs": "smart-tabs"
    },
    "parserOptions": {
        "ecmaVersion": 2018,
        "sourceType": "module"
    },
    "plugins": [
        "vue"
    ],
    "rules": {
	"no-mixed-spaces-and-tabs": "off",
	"no-console": "off",
	"no-undef": "off",
	"no-unused-vars": "off"
   }
};
